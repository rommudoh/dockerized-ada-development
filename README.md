# Dockerized Ada development

This is an example project using Docker for building the source code.

# Usage

## Building the project

`docker-compose up build`

## Running the tests

`docker-compose up test`

## Cleaning the project

`docker-compose up clean`
